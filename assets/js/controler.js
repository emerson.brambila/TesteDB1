"use strict";

/*jogodamemoriaApp Controles*/

var jogodamemoriaApp = angular.module('jogodamemoriaApp', []);

jogodamemoriaApp.factory('game', function() {
	var imgCartas = ['castor', 'dog', 'elefante', 'foca', 'galo', 'gato', 'leao', 'pinguin'];

	return new Game(imgCartas);
});

jogodamemoriaApp.controller('GameCtrl', function GameCtrl($scope, game) {
  $scope.game = game;
});

jogodamemoriaApp.directive('mgCard', function() {
	return {
		restrict: 'E',

    template: '<div class="container">' +
    			'<div class="img-cartas" ng-class="{flipped: carta.flipped}">' +
	    			'<img class="frente" ng-src="./assets/img/verso.png">' +
	    			'<img class="verso" ng-src="./assets/img/{{carta.title}}.png">' +
    			'</div>' +
    		  '</div>',
    scope: {
    	carta: '='
    }
	}
});

/*Fim jogodamemoriaApp Controles*/