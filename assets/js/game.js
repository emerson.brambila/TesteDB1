"use strict";

/*Logica do Jogo e Mensagens*/

function Carta(title) {
	this.title = title;
	this.flipped = false;
}

Carta.prototype.flip = function() {
	this.flipped = !this.flipped;
}

function Game(imgCartas) {
  var cartaDeck = makeDeck(imgCartas);

  this.grid = makeGrid(cartaDeck);
  this.message = Game.MENSAGEM_AO_CLICAR;
  this.unmatchedPairs = imgCartas.length;

  this.flipCarta = function(carta) {
    if (carta.flipped) {
      return;
    }

    carta.flip();

    if (!this.firstPick || this.secondPick) {

      if (this.secondPick) {
        this.firstPick.flip();
        this.secondPick.flip();
        this.firstPick = this.secondPick = undefined;
      }

      this.firstPick = carta;
      this.message = Game.MENSAGEM_MAIS_UMA;

    } else {

      if (this.firstPick.title === carta.title) {
        this.unmatchedPairs--;
        this.message = (this.unmatchedPairs > 0) ? Game.MENSAGEM_ACERTO : Game.MENSAGEM_VENCEU;
        this.firstPick = this.secondPick = undefined;
      } else {
        this.secondPick = carta;
        this.message = Game.MENSAGEM_ERRO;
      }
    }
  }
}

Game.MENSAGEM_AO_CLICAR = 'Clique em uma carta!';
Game.MENSAGEM_MAIS_UMA = 'Clique em outra carta :)'
Game.MENSAGEM_ERRO = 'Errou, tente de novo :)';
Game.MENSAGEM_ACERTO = 'Ótimo você acertou continue assim!';
Game.MENSAGEM_VENCEU = 'Você é um vencedor!';

/*Array com as cartas*/

function makeDeck(imgCartas) {
  var cartaDeck = [];
  imgCartas.forEach(function(name) {
    cartaDeck.push(new Carta(name));
    cartaDeck.push(new Carta(name));
  });

  return cartaDeck;
}


function makeGrid(cartaDeck) {
  var gridDimension = Math.sqrt(cartaDeck.length),
      grid = [];

  for (var row = 0; row < gridDimension; row++) {
    grid[row] = [];
    for (var col = 0; col < gridDimension; col++) {
        grid[row][col] = removeRandomCarta(cartaDeck);
    }
  }

  return grid;
}


function removeRandomCarta(cartaDeck) {
  var i = Math.floor(Math.random()*cartaDeck.length);
  return cartaDeck.splice(i, 1)[0];
}